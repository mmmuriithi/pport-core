<?php
namespace pPort;
class Provider
{	
	public static $accessors=array();

	

	public static function __callStatic($method, $args) 
	{
		return call_user_func_array(array(static::fetchInstance($method,$args), $method), $args);
	}

	public function __call($method, $args) 
	{
		return call_user_func_array(array(static::fetchInstance($method,$args), $method), $args);
	}


	public static function fetchInstance($method,$args)
	{
		$accessor=new static();
		$service=$accessor->register();
		$alias=$accessor->alias;

		

		if(is_object($service)) return $service;
		if(!isset(I()->resolvers[$alias])) 
		{
			I()->resolvers[$alias]=new $service();
		}
		return I()->resolvers[$alias];
	}


}