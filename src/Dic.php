<?php
namespace pPort;
class Dic
{
	public $resolvers=array();
	public $params;
	public $services=array();


	public function __construct()
	{
		$this->services=include('services.php');
		$this->register($this->services);
	}

	public function set($handle,$key,$value=false)
	{
		$this->resolvers_params[$handle][$key]=$value;
	}

	public function register($handle,$resolver=false,$autoload=false)
	{
		if(is_array($handle))
		{
			foreach($handle as $service_alias=>$actual_service)
			{
				$autoload=false;
				if(is_array($actual_service))
				{
					$autoload=isset($actual_service[1])?$actual_service[1]:false;
					$actual_service=$actual_service[0];
				}
				$this->register($service_alias,$actual_service,$autoload);
			}
		}
		else
		{

			if(!array_key_exists($handle,$this->resolvers))
			{
				if($resolver)
				{
					$this->resolvers[$handle]=$resolver;
				}
				else
				{
					if(isset($this->services[$handle]))
					{
						$this->register($handle,$this->services[$handle],$autoload);
					}
				}
			}
			
			
		}
		return $this;
	}


	public function load($handle,$params=array())
	{
		if(isset($this->resolvers[$handle]))
			{


				$resolver=$this->resolvers[$handle];

				if(is_object($resolver))
				{
					return $this->resolvers[$handle];
				}
				else
				{
					if(!class_exists($handle))
					{
						class_alias($resolver,$handle);	
					}
					return new $handle();
					
				}
				 
			}
			else
			{

				//Exception Service Not Found
				die('Service Not Found '.$handle);
			}
		}



	public function __call($method, $args) {

	    if (array_key_exists($method,$this->resolvers)) 
	    {
	      return $this->load($method,$args);
	    }	  
	}

	public function __get($var) 
	{

		if (array_key_exists($var,$this->resolvers))
		{
			return $this->load($var);
	    }
	    else
	    {
	    	$this->register($var)->load($var);
	    	

	    }
  	}

  	

}
